# 8.6 (2022-03-03) by Al Pon

### New Features
* Ported from PyQt4 to PyQt5
* Added file dialog file type/extension filters for open & save dialog widgets

### Bug Fixes
* Replaced 'is'/'is not' with == / != for literal equations
* Other minor bugs
